package org.BenAbderrahmane.exo1;

import java.io.Serializable;


public class Client implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5218769315400417152L;
	/**
	 * 
	 */

	private long ID=serialVersionUID;
	private String nom;
	private String prenom;
	private int anneeNaissance;
	private String adresse;
	private long tel;
	public Client(String nom, String prenom, int anneeNaissance, String adresse, int tel) {
		super();
		ID =serialVersionUID ;
		this.nom = nom;
		this.prenom = prenom;
		this.anneeNaissance = anneeNaissance;
		this.adresse = adresse;
		this.tel = tel;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAnneeNaissance() {
		return anneeNaissance;
	}
	public void setAnneeNaissance(int anneeNaissance) {
		this.anneeNaissance = anneeNaissance;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	@Override
	public String toString() {
		return "Client [ID=" + ID + ", nom=" + nom + ", prenom=" + prenom + ", anneeNaissance=" + anneeNaissance
				+ ", adresse=" + adresse + ", tel=" + tel + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ID ^ (ID >>> 32));
		result = prime * result + ((adresse == null) ? 0 : adresse.hashCode());
		result = prime * result + anneeNaissance;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		result = prime * result + (int) (tel ^ (tel >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Client other = (Client) obj;
		if (ID != other.ID)
			return false;
		if (adresse == null) {
			if (other.adresse != null)
				return false;
		} else if (!adresse.equals(other.adresse))
			return false;
		if (anneeNaissance != other.anneeNaissance)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (tel != other.tel)
			return false;
		return true;
	}


	

	
	
}
