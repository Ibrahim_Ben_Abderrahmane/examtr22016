package org.BenAbderrahmane.exo1;

import java.io.Serializable;

public class Comptes implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1485222536342795343L;

	private long ID=serialVersionUID;
	Client client;
	private float solde;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (ID ^ (ID >>> 32));
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + Float.floatToIntBits(solde);
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Comptes other = (Comptes) obj;
		if (ID != other.ID)
			return false;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (Float.floatToIntBits(solde) != Float.floatToIntBits(other.solde))
			return false;
		return true;
	}
	public Comptes(Client client, float solde) {
		super();
		this.client = client;
		this.solde = solde;
	}
	public long getID() {
		return ID;
	}
	public void setID(long iD) {
		ID = iD;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public float getSolde() {
		return solde;
	}
	public void setSolde(float solde) {
		this.solde = solde;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public boolean crediteCompte(final long serialVersionUID,float sommeACrediter){
		if(serialVersionUID==ID){
			solde=solde+sommeACrediter;
			return true;
		
		}
		return false;
	}
	public boolean debiteCompte(final long serialVersionUID,float sommeADebiter){
		if(serialVersionUID==ID){
			if(solde-sommeADebiter>=100){
				solde=solde-sommeADebiter;
				return true;
			}
			else{
					return false;
				}
		}
		return false;
	}
	@Override
	public String toString() {
		return "Comptes [ID=" + ID + ", client=" + client + ", solde=" + solde + "]";
	}


}
