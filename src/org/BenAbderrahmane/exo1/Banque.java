package org.BenAbderrahmane.exo1;

import java.util.Set;

public class Banque {
	Set<Comptes> listeComptes;
	
	
	public Comptes createCompte(Client client,float soldeInitail){
		Comptes compte = null;
		if(client!=null){
			    compte=new Comptes(client,soldeInitail);
				listeComptes.add(compte);
		}
		return compte;
	}
	


	public Banque(Set<Comptes> listeComptes) {
		super();
		this.listeComptes = listeComptes;
	}



	public Set<Comptes> getAllCompte(){
		if(listeComptes.isEmpty()!=true)
			return listeComptes;
		return null;
	 
	}
	public Set<Comptes> findCompte(String nom){
		if(listeComptes.isEmpty()!=true){
			for(Comptes c: listeComptes){
				if(c.getClient().getNom().equals(nom)){
					listeComptes.add(c);
					return listeComptes;
				}
			}
		
		}
		return null;
	}
	public Comptes getCompteById(int id){
		Comptes compte=new Comptes(null,0);
		if(listeComptes.isEmpty()!=true){
			for(Comptes c: listeComptes){
				if(c.getID()==id){
				return compte;
				}
			}
		}
		return null;
	}

	public Comptes debiteCompteById(int id, float sommeADebiter){
		Comptes compte = null;
		if(listeComptes.isEmpty()!=true){
			for(Comptes c: listeComptes){
				if(c.getID()==id){
					if(c.getSolde()-sommeADebiter>100)
						compte=new Comptes(c.getClient(),c.getSolde()-sommeADebiter);
						return compte;
				}
			}
		}
		return null;
	}
	public Comptes crediteCompteById(int id, float sommeACrediter ){
		Comptes compte;
		if(listeComptes.isEmpty()!=true){
			for(Comptes c: listeComptes){
				if(c.getID()==id){
				compte=new Comptes(c.getClient(),c.getSolde()+sommeACrediter);
				return compte;
				}
			}
		}
		return null;
	}
	public boolean removeCompteById(int id){
		if(listeComptes.isEmpty()!=true){
			for(Comptes c: listeComptes){
				if(c.getID()==id){
				c=null;
				return true;
				}
			}
		}
		return false;
	}
	public void virements(int id1, int id2, int somme){
		Comptes compte1 = null;
		Comptes compte2 = null;
		if(listeComptes.isEmpty()!=true){
			for(Comptes c1: listeComptes){
				if(c1.getID()==id1){
				compte1=new Comptes(c1.getClient(),c1.getSolde());
				}
			}
			for(Comptes c2: listeComptes){
				if(c2.getID()==id2){
				compte2=new Comptes(c2.getClient(),c2.getSolde());
				}
			}
			compte2.debiteCompte(id2, somme);
			compte1.crediteCompte(id1, somme);
		}
	}

	public void virementsinverse(int id2, int id1, int somme){
		Comptes compte1 = null;
		Comptes compte2 = null;
		if(listeComptes.isEmpty()!=true){
			for(Comptes c1: listeComptes){
				if(c1.getID()==id1){
				compte1=new Comptes(c1.getClient(),c1.getSolde());
				}
			}
			for(Comptes c2: listeComptes){
				if(c2.getID()==id2){
				compte2=new Comptes(c2.getClient(),c2.getSolde());
				}
			}
			compte1.debiteCompte(id1, somme);
			compte2.crediteCompte(id2, somme);
		}
	}
}

